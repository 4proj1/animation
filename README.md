## Qu'est-ce que c'est ?

Projet d'animation: Emulation d'un client dans un magasin connecté.

### Library used

Browser-Sync, Three.js, WEBGL, Twig, Doctrine ORM, Symfony.

### Développer ce nouveau projet

Ce nouveau projet que vous venez de créer contiens désormais des outils pour aider le développement.

Le Makefile permet de faciliter de nombreuses choses :
- make deploy-dev : permet de créer un container symfony et un container mariadb liés ensemble. Après cette commande, votre site sera accessible à l'addresse localhost:8000
- make migration : permet de lancer une migration sur l'image symfony (console make:migration) et ainsi déployer les changements apportés à la base de données via doctrine.
- make push : construit l'image docker et la push dans le registry

/!\ ADAPTEZ CE README ET LA DOC A VOTRE PROJET /!\