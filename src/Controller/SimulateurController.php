<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class SimulateurController extends AbstractController
{
    /**
     * @Route("/simulateur", name="simulateur")
     */
    public function index()
    {
        return $this->render('simulateur/index.html.twig', [
            'controller_name' => 'SimulateurController',
        ]);
    }
}
