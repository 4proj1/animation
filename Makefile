deploy-dev:
	docker container prune
	docker volume prune
	docker-compose up

migration:
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console make:migration
	docker exec toutpareilusermanager_usermanager_1 php /app/project/bin/console doctrine:migrations:migrate

push:
	docker build . -t animation
	docker image tag animation c-est.party:5000/animation
	docker push c-est.party:5000/animation